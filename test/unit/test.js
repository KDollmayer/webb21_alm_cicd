const assert = require('assert');
const { sumOf, deleteSum } = require('../../routes/calc');

describe('Array', function () {
  describe('#indexOf()', function () {
    it(
      'should return -1 when the value is not present',
      function () {
        assert.equal([1, 2, 3].indexOf(4), -1);
      },
    );
  });
  describe('sumOf()', function () {
    it('SumOF', function () {
      const val1 = '1';
      const val2 = '1';
      const sum = sumOf(val1, val2);
      assert.strictEqual(sum, 2);
    });
  });
  describe('deleteSum()', function () {
    it('DeleteSum', function () {
      const sum = deleteSum();
      assert.equal(sum, 0);
    });
  });
});
